<?php

/**
 * Interface to be implemented by all data handlers for different data providers.
 */
interface DataProviderInterface{

    /** Sets posted data from data provider to local variable.*/
    public function setData($data);

    /** Put checks on the posted data for validity.*/
    public function dataValidator($postedData, $mandatoryFields);

    /** Push/Store data in the chosen datastore.*/
    public function storeData($key, $value);

    /** Starts functionin of data handler from this function.*/
    public function run();
}
