<?php

/**
 * Class to provide utility functions for data handling/processing.
 * Several data handlers can extend this class to share common functions.
 */
class ProfileUtility{

    /**
     * Function to perform basic checks on data to validata.
     * @var $postedData array Data posted by data provider.
     * @var $mandatoryFieldList array List of fields which should be present in data.
     * @return boolean TRUE if data is valid, FALSE otherwise.
     */
    protected function dataValidator($postedData, $mandatoryFieldList){
        if(empty($postedData) or is_null($postedData)){
            return FALSE;
        }else{
            foreach($mandatoryFieldList as $field){
                if(!isset($postedData[$field]) or is_null($postedData[$field]) or empty($postedData[$field])){
                    return FALSE;
                }
            }
            return TRUE;
        }
    }

    /**
     * @var $pixelData array Pixel from posted data.
     * @return STRING returns a uid string.
     */
    protected function fetchUUID($pixelData){
        $uuidKey = "idfa";
        $uuidValue = NULL;
        if(isset($pixelData[$uuidKey]) and !is_null($pixelData[$uuidKey]) and !empty($pixelData[$uuidKey])){
            $uuidValue = $pixelData[$uuidKey];
        }
        return $uuidValue;
    }

}
?>
