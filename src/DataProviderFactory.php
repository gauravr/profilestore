<?php

/**
 * This class makes object of desired data provider. And the object is returned to be used for further operation.
 */

class DataProviderFactory{

    /**
     * @return object Returns object of data handler for respective data provider.
     */
    public static function getDataProviderHandler($dataProviderId){
        $dataProviderObj = Null;
        if($dataProviderId){
            switch($dataProviderId){
            case "bk":
                include __DIR__."/BluekaiParser.php"; 
                $dataProviderObj = new BluekaiParser($dataProviderId);
                break;
            default:
                $dataProviderObj = Null;
            }
        }
        return $dataProviderObj;
    }
}
?>
