<?php

require_once __DIR__."/ProfileUtility.php";
require_once __DIR__."/DataProviderInterface.php";
require_once __DIR__."/../lib/cachingLayer/aerospikeStore.php";

/**
 * Class to handle data posted by bluekai on endpoint.
 */
class BluekaiParser extends ProfileUtility  implements DataProviderInterface{

    private $data = Null;
    private $cacheObj;
    private $dataProviderId;
    private static $mandatoryFieldList = array( "BkUuid", "CategoryId", "idfa");

    /**
     * Constructor to have object of caching layer instead.
     */
    public function __construct($dataProviderId){
        $this->dataProviderId = $dataProviderId;
        $this->cacheObj = new aerospikeStore();
    }

    /**
     * Sets posted data from data provider to local variable.
     * @var $data array Data posted by luekai on endpoint. 
     */
    public function setData($data){
        $this->data = $data;
    }

    /**
     *
     */
    public function generateKey($partialKey){
      $key = NULL;
      if(!is_null($partialKey) and !empty($partialKey)){
        $key = $partialKey.'_'.$this->dataProviderId;
      }
      return $key;
    }

    /**
     * Push/Store data in the chosen datastore.
     * @var $key String Key for which value has to be stored.
     * @var $value array Value to be stored.
     * @return boolean|array|String Status of the set operation.
     */
    public function storeData($key, $value){
        $key = $this->generateKey($key);
        $retVal = $this->cacheObj->set($key, $value);
        return $retVal;
    }

    public function fetchData($key){
      #$key = $this->generateKey($key);
      return $this->cacheObj->fetch($key);
    }

    public function dataValidator($postedData, $mandatoryFieldList){
        return parent::dataValidator($postedData, $mandatoryFieldList);
    }

    /**
     * Filters-IN valid pixel from data.
     * @return array Returns valid pixels.
     */
    public function filterInValidData(){
        $pixelsData = $this->data["Pixels"];
        $validData = array();
        if(empty($this->data) or is_null($this->data)){
            return array();
        }else{
            foreach($pixelsData as $pixel){
                if($this->dataValidator($pixel, self::$mandatoryFieldList)){
                    $validData[] = $pixel;
                }
            }
        }
        return $validData;
    }

    /**
     * Function initiates the flow of operations to be performed on posted data.
     */
    public function run(){
        $validPixelData = $this->filterInValidData();
        foreach($validPixelData as $pixel){
            $uuid = $this->fetchUUID($pixel);
            if(!is_null($uuid)){
                $retVal = $this->storeData($uuid, $pixel);
            }
        }
    }
} 
?>
