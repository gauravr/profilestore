<?php

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

/**
 * Fetches Data Provider Id from URL.
 * @var String Url on which data has been posted by data provider.
 * @return String Returns DataProvider Id.
 */ 
function getCallerFromURL($url){
    return "bk";
}

try
{
    $postdata = file_get_contents("php://input");
    $postdata = json_decode($postdata, TRUE);
    $caller = getCallerFromURL("PASS_URL_HERE");
    if($caller and $postdata){
        require __DIR__."/DataProviderFactory.php";
        $callerObj = DataProviderFactory::getDataProviderHandler($caller);
        $callerObj->setData($postdata);
        $callerObj->run();
        echo "Success\n";
    }
}
catch ( Exception $e)
{
    echo sprintf("<br>failure</br>\n%s\n", $e->getMessage());
}
?>
