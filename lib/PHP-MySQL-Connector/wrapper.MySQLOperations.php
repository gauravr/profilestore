<?php
require_once __DIR__.'/class.MySQLOperations.php';
require_once __DIR__.'/../../vdopiaLib/PathInfo.php';

class wrapperMySQLOperations
{
	private $configFile;
	private $dbConnectionObject;
	private static $instance;

	public function __construct($configFile='')
	{
		$this->setConfig($configFile);
		$this->setDbInstance();
		self::$instance = $this;
	}

	public static function getSingleton($configFile='')
	{
		if(is_null(self::$instance))
		{
			self::$instance = new wrapperMySQLOperations($configFile);
		}
		return self::$instance;
	}

	public static function getDbConnectionObject()
	{
		$thisObject = self::getSingleton();
		return $thisObject->dbConnectionObject;
	}

	private function setDbInstance()
	{
		$dbConfig = parse_ini_file($this->configFile, TRUE);
		$dbConfig = $dbConfig['Database'];
		$database = isset($dbConfig['db_name'])?$dbConfig['db_name']:'';
		$user = isset($dbConfig['user_name'])?$dbConfig['user_name']:'';
		$passwd = isset($dbConfig['password'])?$dbConfig['password']:'';
		$ip = isset($dbConfig['host'])?$dbConfig['host']:'';
		$port = isset($dbConfig['port'])?$dbConfig['port']:'';

        if($port!='')
        {
            $ipPort = sprintf("%s:%s", $ip, $port);
        }
        else
        {
            $ipPort = $ip;
        }

		$this->dbConnectionObject = new MySQLOperations($database, $user, $passwd, $ipPort);
	}

	private function setConfig($configFile)
    {
		if(!is_string($configFile) || $configFile == '')
		{
			$pathInfo = PathInfo::getPathInfo();
			$this->configFile = $pathInfo["CONF_DIR"]."/conf.ini"; 
		}
		else
		{
			$this->configFile = $configFile;
		}
		if(!is_readable($this->configFile))
		{
			throw new Exception("Config file to connect to mysql not found:". $this->configFile);
		}
	}

	public function Execute($query)
	{
        $resultSet = array();
		$sqlResult  = $this->dbConnectionObject?$this->dbConnectionObject->ExecuteSQL($query):FALSE;
		if( $sqlResult !== FALSE and is_array($sqlResult))
		{
			if(!isset($sqlResult[0]))
			{
				$resultSet[0] = $sqlResult;
			}
			else
			{
				$resultSet =$sqlResult;
			}
		}
		return $resultSet;
	}

	function __destruct()
	{
		unset($this->dbConnectionObject);
	}
}
