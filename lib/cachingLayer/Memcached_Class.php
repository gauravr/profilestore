<?php

require_once __DIR__."/cachingInterface.php";

/**
    * This class Memcached_Class implements the cachingInterface. Memcached cache is implemented as caching solution.
    * @author Gaurav Ruhela
   */

class Memcached_Class implements cachingInterface
{
    /**
     * @var array $confArray  Setting array.
     */
    private $memcachedObj;

    /**
     * @var object $memcachedObj Object of type memcache to take care of singleton patterns.
     */
    private $confArray;

    /**
     * @param array $confArray Confirguration like ip, port are passed to connect to the cache.
     * Loads default config if not passed, when used as a standalone module.
     */
    public function __construct($confArray = NULL)
    {
        $this->confArray = (is_null($confArray) or empty($confArray))?$this->loadDefaultConfig():$confArray;

        $this->memcachedObj = new Memcached("AWS");
        if (!count($this->memcachedObj->getServerList())) 
        {
            $this->memcachedObj->setOption(Memcached::OPT_DISTRIBUTION, Memcached::DISTRIBUTION_CONSISTENT);
            $this->memcachedObj->setOption(Memcached::OPT_LIBKETAMA_COMPATIBLE, $this->confArray['libketama_compatible']);
            $this->memcachedObj->setOption(Memcached::OPT_CONNECT_TIMEOUT, $this->confArray['connect_timeout']);
            $this->memcachedObj->setOption(Memcached::OPT_RECV_TIMEOUT, $this->confArray['recv_timeout']);
            if ($this->confArray['dynamic_mode'] and 0) # Comment gaurav
            {
                $this->memcachedObj->setOption(Memcached::OPT_CLIENT_MODE, Memcached::DYNAMIC_CLIENT_MODE);
            } 
            $this->memcachedObj->addServer($this->confArray['ip'], $this->confArray['port']);
        }
    }

    /**
     * Function to load default config
     */
    private function loadDefaultConfig()
    {
        require_once __DIR__.'/CacheSetting.php';
        return CacheSetting::$confArrayDefault['memcache'];
    }


    /**
     * @param mixed $key Key whose value has to be stored.
     * @param mixed $value Values for the corresponding variable $key.
     * @param float $ttl Time to live for key-value pair. $ttl if not passed specifically, is used from default config. Application can pass 0, as never expire.
     * @param boolean $compress Compress the values to store.
     */
    public function set($key, $value, $ttl=NULL, $compress=0)
    {
        $ttl = ($ttl===NULL)?$this->confArray['ttl']:$ttl;
        return $this->memcachedObj->set($key, $value, $ttl);
    }


    /**
     * @param array $keyValue Array of key value pairs.
     * @param float $ttl Time to live for key-value pair.
     * @param boolean $compress Compress the values to store.
     */
    public function setMulti($keyValue, $ttl=NULL, $compress=0)
    {
        $ttl = ($ttl===NULL)?$this->confArray['ttl']:$ttl;
        return $this->memcachedObj->setMulti($keyValue, $ttl);
    }

    /**
     * @param mixed $key Key for which values has to be retrieved.
     */
    public function fetch($key)
    {
        return $this->memcachedObj->get($key);
    }

    /**
     * @param array $keys Array of keys to be retrieved.
     */
    public function fetchMulti($keys)
    {
        return $this->memcachedObj->getMulti($keys);
    }

    /**
     * @param $key mixed Key to be removed 
     */
    public function removeKey($key)
    {
        return $this->memcachedObj->delete($key);
    }

    /**
     * @param $keys array Array of keys to be removed.
     */
    public function &removeMulti($keys)
    {
        $retVal = $this->memcachedObj->deleteMulti($keys); // Return all keys with TRUE/FAlSE on success/failure of deletion of that key
        $tmp = array();
        foreach($retVal as $k=>$v)
        {
            if($v===TRUE)
            {
                $tmp[] = $k;
            }
        }
        return $tmp;
    }
}	
?>
