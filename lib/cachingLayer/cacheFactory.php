<?php

class cacheFactory
{
    public static $cacheFactoryObj;

    private $conf;
    private $cacheObjectInstance;

    private function __construct($cacheLayer,  $confArray=array())
    {
        $this->loadConfig($cacheLayer, $confArray);
        self::$cacheFactoryObj = $this;
    }

    public static function getSingleton( $cacheLayer,  $confArray=array())
    {
        if(!isset(self::$cacheFactoryObj))
        {
            self::$cacheFactoryObj = new cacheFactory($cacheLayer, $confArray);
        }
        return self::$cacheFactoryObj;
    }

    private function loadConfig($cacheLayer, $confArray)
    {
        if(isset($confArray['ip']) and isset($confArray['port']))
        {
            $this->conf = array($cacheLayer => $confArray);
        }
        else
        {
            /* cachelayer should exist as call to this layer comes from cachinglayer/multilayercaching, whicch filters non supported caching layers.*/
            require_once __DIR__.'/CacheSetting.php';
            $this->conf = CacheSetting::$confArrayDefault;
        }

        if(!isset($this->conf) or empty($this->conf))
        {
            require_once __DIR__.'/ErrorCodes.php';
            throw new exception(ERRORCODES::INVALID_CACHING_LAYER);
        }
    }

    private function cacheObjectKey($cacheLayer)
    {
        /* Empty ip, port for caching layers like apc*/
        /* Cache Object are saved by this key, to support same type cache like redis on diff ports*/
        $ip = (isset($this->conf[$cacheLayer]['ip']))?$this->conf[$cacheLayer]['ip']:'';
        $port = (isset($this->conf[$cacheLayer]['port']))?$this->conf[$cacheLayer]['port']:'';

        $cacheObjectKey = $cacheLayer.'_'.$ip.'_'.$port;
        return $cacheObjectKey;
    }

    public static function getCacheObject($cacheLayer, $confArray=array())
    {
        $cacheLayer = strtolower($cacheLayer);
        $thisObject = self::getSingleton($cacheLayer, $confArray);
    
        $cacheObjectKey = $thisObject->cacheObjectKey($cacheLayer);
        if(isset($thisObject->cacheObjectInstance[$cacheObjectKey]))
        {
            return $thisObject->cacheObjectInstance[$cacheObjectKey];
        }

        $cacheObj = NULL;
        switch($cacheLayer)
        {
            case "apc":
                require_once __DIR__."/apcCache.php";
                $cacheObj = new apcCache();
                break;
            case "memcache":
                require_once __DIR__."/Memcached_Class.php";
                $cacheObj = new Memcached_Class($thisObject->conf[$cacheLayer]);
                break;
            default:
                require_once __DIR__."/apcCache.php";
                $cacheObj = new apcCache();
                break;
        }
        $thisObject->cacheObjectInstance[$cacheObjectKey] = $cacheObj;
        return $cacheObj;
    }
}
?>
