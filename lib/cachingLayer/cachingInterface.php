<?php
/**
 * This files defines an interface which has to be implemented by different caching solutions.
 * @author Gaurav Ruhela
 */

interface cachingInterface
{
    /**
     * Store the variable using this name. Keys are cache-unique, so storing a second value with the same key will overwrite the original value.
     * If no ttl is supplied (or if the ttl is 0), the value will persist until it is removed from the cache manually, or otherwise fails to exist in the cache.
     * @param mixed $key Key whose value has to be stored.
     * @param mixed $value Values for the corresponding variable $key.
     * @param float $ttl Time to live for key-value pair.
     * @param boolean $compress Compress the values to store.
     * @return boolean TRUE on success and FALSE on failure.
     */
    public function set($key, $value, $ttl=0, $compress=0);

    
    /**
     * Sets an array of key values pair in cache.
     * @param array $keyValue Array of key value pairs.
     * @param float $ttl Time to live for key-value pair.
     * @param boolean $compress Compress the values to store.
     * @return  boolean Returns TRUE if all keys are set successfully else FALSE.
     */
    public function setMulti($keyValue, $ttl=0, $compress=0);


    /**
     * Fetches a stored variable from the cache.
     * @param mixed $key Key for which values has to be retrieved.
     * @return mixed|boolean Found item value or FALSE on failure.
     */
    public function fetch($key);


    /**
     * Fetches multli keys at once from cache.
     * @param array $keys Array of keys to be retrieved.
     * @return array|boolean Array of found items or FALSE on failure.
     */
    public function fetchMulti($keys);

    
    /**
     * Removes a stored variable from the cache
     * @return boolean TRUE on success and FALSE on failure.
     * @param mixed $key can be an array too.
     */
    public function removeKey($key);


    /**
     * Removes a list of stored keys from cache.
     * @param $keys array Array of keys to be deleted.
     * @return array Array of keys which were deleted successfully.
     */
    public function removeMulti($keys);
}
