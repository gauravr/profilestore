<?php

require_once __DIR__.'/CacheSetting.php';

class dbLayer
{
    private $cacheStore;
    private $dbConnection;
    private $cachingObject;
    private static $thisObject;

    const DB_RESULT_TTL = 600; #10 Minutes

    public function __construct($cacheStore=NULL)
    {
        $this->cacheStore = ($cacheStore===NULL)?CacheSetting::$defaultCache:$cacheStore;
    }

    public static function getSingleton($cacheStore=NULL)
    {
        if(!(isset(self::$thisObject)))
        {
            self::$thisObject = new dbLayer($cacheStore);
        }
        return self::$thisObject;
    }

    private function queryExecuteDB($query)
    {
        if(!isset($this->dbConnection))
        {
            require_once __DIR__."/../externLib/PHP-MySQL-Connector/wrapper.MySQLOperations.php";
            $this->dbConnection = wrapperMySQLOperations::getSingleton();
        }

        return $this->dbConnection->Execute($query);
    }

    private function queryExecuteCache($query, $ttl)
    {
        if(!isset($this->cachingObject))
        {
            require_once __DIR__."/cacheFactory.php";
            $this->cachingObject = cacheFactory::getCacheObject($this->cacheStore);
        }

        $output = $this->cachingObject->fetch($query); 
        if(is_null($output) or ($output===FALSE))
        {
            $output = $this->queryExecuteDB( $query);
            $retVal = $this->cachingObject->set($query, $output, $ttl);
        }
        return $output;       
    }

    public function Execute($query, $useCache=0, $ttl=NULL)
    {
        $ttl = ($ttl===NULL)?self::DB_RESULT_TTL:$ttl;
        if($useCache)
        {
            $output = $this->queryExecuteCache($query, $ttl);
        }
        else
        {
            $output = $this->queryExecuteDB($query);
        }
        return $output;
    }
}
?>
