<?php

/** Defines the default config for various/supported cache layers.
 * @author Gaurav Ruhela
 */
abstract class CacheSetting
{
    /**
     * @var string $defaultCache  Default layer of cache.
     */
    public static $defaultCache = 'apc';

    /**
     * @var array $cachePreference Preference order or lookup order of caches.
     */
   public static $cachePreference = array('apc' => 1, 'memcache' => 2, 'aerospike'=>3);

    /**
     * @var array $confArrayDefault Default setting for cache. Will be overridden if passed by calling class.
     */
    public static $confArrayDefault = array(
        'apc' => array(
            'ttl' => 300
        ),

        'memcache' => array(
            'ttl' => 86400,
            'ip' => 'production-php.jn7bj4.cfg.use1.cache.amazonaws.com',
            'port' => 11211,
            'dynamic_mode' => TRUE,
            'libketama_compatible' => TRUE,
            'connect_timeout' => 50,
            'recv_timeout' => 50
        ),

        'redis_local' => array(
            'ttl' => 86400,
            'ip' => 'localhost',
            'port' => 8194
        ),

        'aerospike' => array(
            "hosts"=>
                array(
                    array(
                        "addr"=>"localhost",
                        "port"=>3000
                    )
                )
            )
    );
}
