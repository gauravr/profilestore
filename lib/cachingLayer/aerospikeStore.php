<?php

class aerospikeStore{

    const NAMESPACE_PROFILE = "test";
    const SET_PROFILE = "UserProfilesStore";
    const READ_TIMEOUT = 5;
    const WRITE_TIMEOUT = 5;

    private $config = array("hosts"=>array(array("addr"=>"localhost","port"=>3000)));
    public $connection;

    function __construct($configArray=NULL){
        if(!is_null($configArray) and is_array($configArray) and is_array($configArray['hosts'])){
            $this->config = $configArray;
        }
        $this->connection = new Aerospike($this->config);
    }


    public function isConnected(){
        return $this->connection->isConnected();
    }

    private function generatePrimaryKey($key){
        return $this->connection->initKey(self::NAMESPACE_PROFILE, self::SET_PROFILE, $key);
    }

    public function set($key, $value, $ttl = 0, $timeOut=20){
        $primaryKey = $this->generatePrimaryKey($key);
        $retVal = $this->connection->put($primaryKey, $value, $ttl, array(Aerospike::OPT_WRITE_TIMEOUT => self::WRITE_TIMEOUT));
        if ($retVal === Aerospike::OK){
            $retVal = TRUE;
        }else{
            $retVal = FALSE;
        }
        return $retVal;
    }

    public function fetch($key){
        $primaryKey = $this->generatePrimaryKey($key);
        $retVal = $this->connection->get($primaryKey, $record);
        #$retVal = $this->connection->get($primaryKey, $record, array(Aerospike::OPT_READ_TIMEOUT => self::READ_TIMEOUT));
        if ($retVal === Aerospike::OK){
            $retVal = TRUE;
            $record = $record['bins'];
        }else{
            $retVal = FALSE;
        }
        return $record;
    }

    public function close(){
        $this->connection->close();
    }
}
/*
$config = array("hosts"=>array(array("addr"=>"localhost","port"=>3000)));
$obj = new aerospikeStore($config);
var_dump(array("isConnected"=>$obj->isConnected()));
$val = array('a'=>"gaurav", 'b'=>"ruhela");
$key = "gauravruhela";
var_dump(array("S"=>$obj->set($key, $val)));
var_dump(array("I"=>$obj->fetch($key)));
 */
?>
