<?php

require_once __DIR__."/cachingInterface.php";
require_once __DIR__."/cacheFactory.php";
require_once __DIR__."/CacheSetting.php";
require_once __DIR__."/../globals.php";

define("ALWAYS_PERFORM_ON_ALL_CACHE_LAYERS", TRUE);

/**
 * MultiLevelCache is to facilitate operations on multiple cacing layers, if needed.
 * @author Gaurav Ruhela
 */
class MultiLevelCache implements cachingInterface
{
    /**
     * @var array $cacheLayerArray List of caching layers on which operations have to be performed.
     * @var array $cacheLayerRespectiveConfig Array of configurations for respective cache layers.
     * @var object $MultiLevelCacheObject Object of MultiLevelCache class.
     */
    private $cacheLayerArray = array();
    private $cacheLayerRespectiveConfig;
    private static $MultiLevelCacheObject;


    /**
     * @param array|string $cacheLayer Caching layers which are to be used.
     * @param array $cacheLayerRespectiveConfig Array of setting for caching layers.
     */
    public function __construct($cacheLayer, $cacheLayerRespectiveConfig=array())
    {
        $this->cacheLayerArray = is_string($cacheLayer) ? array($cacheLayer) : $cacheLayer;
        $this->cacheLayerRespectiveConfig = $cacheLayerRespectiveConfig;
        // Remove non supported caches passed by client.
        $this->filterNonSupportedCache($this->cacheLayerArray);
    }

    /**
     * Filters non supported cache layers. Use CacheSetting for cachePreference.
     */
    private function filterNonSupportedCache($cacheLayerArray)
    {
        foreach($cacheLayerArray as $cache)
        {
            if(!isset(CacheSetting::$cachePreference[$cache]))
            {
                throw new Exception("Cache: ".$cache." Not Supported.");
            }
        }
    }

    public static function getSingleton($cacheLayer, $cacheLayerRespectiveConfig=array())
    {
        if(!isset(self::$MultiLevelCacheObject))
        {
            self::$MultiLevelCacheObject = new MultiLevelCache($cacheLayer, $cacheLayerRespectiveConfig);
        }
        return self::$MultiLevelCacheObject;
    }

    public function performOperation($functionName, $allArguments, $performOnAllCacheLayers=FALSE)
    {
        //If some operation, for eg fetch goes to second layer, if entries are present in second layer, first layer should be updated with those.
        $retVal = array();
        foreach($this->cacheLayerArray as $cacheLayer)
        {
            $config = (isset($this->cacheLayerRespectiveConfig[$cacheLayer]))?$this->cacheLayerRespectiveConfig[$cacheLayer]:array();
            
            //Singleton pattern is used in cacheFactory, so getting object again is not a problem.
            $cachingObj = cacheFactory::getCacheObject($cacheLayer, $config);

            $retVal[$cacheLayer] = call_user_func_array(array($cachingObj, $functionName), $allArguments);
            if(!$performOnAllCacheLayers)
            {
                /*if Values are fetched for all, then no need to check in other cache layer,break!*/
                $executionSuccess = is_bool($retVal[$cacheLayer])?$retVal[$cacheLayer]===TRUE:(count($retVal[$cacheLayer])==count($allArguments[0]));
                if($executionSuccess)
                {
                    break;
                }
                else
                {
                    if(is_array($allArguments[0]) and !empty($allArguments[0]))
                    {
                        $allArguments[0] = array_diff($allArguments[0], array_keys($retVal[$cacheLayer]));
                    }
                }
            }
        }
        return $retVal;
    }

    public function set($key, $value, $ttl=NULL, $compress=0)
    {
        $retVal = $this->performOperation(__FUNCTION__, func_get_args(), ALWAYS_PERFORM_ON_ALL_CACHE_LAYERS);
        return $retVal;
    }

    public function setMulti($keyValue, $ttl=NULL, $compress=0)
    {
        $retVal = $this->performOperation(__FUNCTION__, func_get_args(), ALWAYS_PERFORM_ON_ALL_CACHE_LAYERS);
        return $retVal;
    }

    public function fetch($key)
    {
        $retVal = $this->performOperation(__FUNCTION__, func_get_args(), !ALWAYS_PERFORM_ON_ALL_CACHE_LAYERS);
        $returnValue = array();
        foreach($this->cacheLayerArray as $cacheLayer)
        {
            if(isset($retVal[$cacheLayer]) and $retVal[$cacheLayer])
            {
                $returnValue = array_replace($retVal[$cacheLayer], $returnValue);
            }
        }
        return $returnValue;
    }

    public function fetchMulti($keys)
    {
        $retVal = $this->performOperation(__FUNCTION__, func_get_args(), !ALWAYS_PERFORM_ON_ALL_CACHE_LAYERS);
        $returnValue = array();
        foreach($this->cacheLayerArray as $cacheLayer)
        {
            $returnValue = array_replace($retVal[$cacheLayer], $returnValue);
        }
        return $returnValue;
    }

    public function removeKey($key)
    {
        $retVal = $this->performOperation(__FUNCTION__, func_get_args(), ALWAYS_PERFORM_ON_ALL_CACHE_LAYERS);
        return $retVal;
    }

    public function removeMulti($keys)
    {
        $retVal = $this->performOperation(__FUNCTION__, func_get_args(), ALWAYS_PERFORM_ON_ALL_CACHE_LAYERS);
        return $retVal;
    }
}
