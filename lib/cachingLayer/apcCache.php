<?php
require_once __DIR__."/cachingInterface.php";

/**
 * This class apcCache implements the cachingInterface. apc cache is implemented as caching solution.
 * @author Gaurav Ruhela
 */

class apcCache implements cachingInterface 
{

    /**
     * @param mixed $key Key whose value has to be stored.
     * @param mixed $value Values for the corresponding variable $key.
     * @param float $ttl Time to live for key-value pair.
     * @param boolean $compress Compress the values to store.
     */
    public function set($key, $value, $ttl=0, $compress=0)
    {
        return apc_store($key, $value, $ttl);
    }

    /**
     * @param array $keyValue Array of key value pairs.
     * @param float $ttl Time to live for key-value pair.
     * @param boolean $compress Compress the values to store.
     */

    public function setMulti($keyValue, $ttl=0, $compress=0)
    {
        $retVal = apc_store($keyValue, $ttl); //Returns array of keys which failed to set
        if(!empty($retVal))
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /**
     * @param mixed $key Key for which values has to be retrieved.
     */
    public function fetch($key)
    {
        return apc_fetch($key);
    }


    /**
     * @param array $keys Array of keys to be retrieved.
     */
    public function fetchMulti($keys)
    {
        return apc_fetch($keys);
        /*
        {
            $retValNew = array();
            foreach($keys as $k)
            {
                if(!isset($retVal[$k]))
                {
                    $retValNew[$k] = NULL;
                }
                else
                {
                    $retValNew[$k] = $retVal[$k];
                }
            }
        }
        return $retValNew;
        */
    }

    /**
     * @param $key mixed Key to be removed
     */
    //Returns successfully removed keys/TRUE for array/string
    public function removeKey($key)
    {
        return apc_delete($key);
    }

    /**
     * @param $keys array Array of keys to be removed.
     */
    public function removeMulti($keys)
    {
        $retVal = apc_delete($keys);
        return array_diff($keys, $retVal);
    }
}
?>
